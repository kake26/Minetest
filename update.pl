#!/usr/bin/perl

# This script will check and update minetest mods as needed. It is not 100% perfect, but it does work.
# Currently, any version checking is done against the mod file generated from a previous run.

# This was a bash script until I determined the level of invoking the ancient gods of the console required to make 
# that work would be just too much

use Storable; 

# I know declaring a empty hash is 100% unnessecary but I am anyway
%conf = {}; # this will be stored used to keep track of stuff

$updfile = "index.json"; # I could use JSON here, but it might be easier to abuse jq
$updlsturl = "https://content.minetest.net/api/packages/"; # give use a json file of everything available
$modpath = "/home/kake26/minetest-5.10.0/mods"; # where your server stores its mods

sub chk_update () {

print "Checking for updates...\n";    

foreach my $key (keys %{ $conf->{"mods"}}) { # the fing arrow -> sigh could have saved so much time

    print "Checking mod $key \n";

    # So we can now check against index2.json 
    
    $author2 = `jq -c '.[] | select(.name =="$key") | .author' index2.json`;
    $author2 =~ s/\"//g;
    
    $release2 = `jq -c '.[] | select(.name =="$key") | .release' index2.json`;
    $release2 =~ s/\"//g;

    chomp($author2);
    chomp($release2);

    if ($conf->{"mods"}{$key}{"release"} != $release2) {
        print "Mod $key current release: " . $release2 . " installed: " . $conf->{"mods"}{$key}{"release"} ."!\n";
        # we should trigger the updater here
        
        &do_update($key,$release2,$author2);
    }else{
        print "Mod $key up to date\n";
    }

}
}

sub get_ready (){
$mods = `find $modpath -type d -maxdepth 1 -print`; # -print is there for a reason, find saved a huge amount of time
@mods = split("\n",$mods);
foreach(@mods){
    @tmods = split("\/",$_);
    $tmname = pop(@tmods);
    if ($tmname eq "mods"){
        next;
    }
    # At this point we should have a mod's name
    $author = `jq -c '.[] | select(.name =="$tmname") | .author' index.json`; #jq is a time saver
    $author =~ s/\"//g;
    chomp($author);
    $release = `jq -c '.[] | select(.name =="$tmname") | .release' index.json`;
    $release =~ s/\"//g;
    chomp($release);
    $conf{"mods"}{$tmname}{"author"} = $author;
    $conf{"mods"}{$tmname}{"release"} = $release;
}
store \%conf, 'mods';
return;
}

sub do_update ($mod,$ver,$author) {
# https://content.minetest.net/packages/TenPlus1/mob_horse/download/
# https://content.minetest.net/packages/jp/i3/releases/14157/download/
   $mod = $_[0];
   $ver = $_[1];
   $author = $_[2];
 
   system("wget -O $mod.zip https://content.minetest.net/packages/$author/$mod/releases/$ver/download/");
   system("unzip -o $mod.zip");
   # print "Update info $mod $ver $author\n";
}

sub update_files () {
# This is triggered when mod updates are detected
system("rm index.json");
system("cp index2.json index.json");
system("rm mods");
undef(%conf);
&get_ready();
return;
}

# Maybe get ready and check update should be called every time

if (-e "mods"){
    
    # load data and business as usual

    print "Loading data\n";
    $conf = retrieve('mods');
    system("wget -O index2.json $updlsturl"); # Don't need a whole sub for one command
    chk_update();
    update_files();
}else{
    # if the mods file doesn't exist then we need to create it
    print "Creating mods file\n";
    system("wget -O index.json $updlsturl");
    get_ready();
    print "Done. You will need to run me again to update mods.\n";
 }
