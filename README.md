# Luanti (Formerly Minetest)

Luanti related code.

# Branches

MAIN

Will have the most recent working tools. Testing dual branches.

DEV

Contains works in progress and the latest testing code.

# What these scripts are

buildstable.bash - Builds the latest stable version of the minetest server. I use this on my VPS where I host my minetest server.

server.bash - Script to start/stop and baby sit the server. Runs via cron to restart it in case of crash.

update.pl - Perl script can be run in the mods folder to update mods. Wrote this because manually checking and doing it was a massive pain. It does require jq to be installed. Its best installed after a fresh setup as it uses index.json to keep track of what mods need to be updated when new versions are released.

# Status

As of 1/12/25:

Got around to updating things to 5.10 finally. I'me using it again to manage my Minetest server. I am going to go through with the plans of updating and expanding this finally. I know I've been saying that a while, but since I'm using it again its time to finally do this. 

As of 4/15/23:

Some minor updates for minetest 5.7.0. All code was given a test as part of my server's update to minetest 5.7.0. Seems to work just fine for what I made it for. Expanding the script to include more mod management realted functions is certainly something that will happen as I'm tired of adding new mods manually. Additional updates to the build script for optimized building. 

As of 1/5/23:

Dev branch updated to the latest version of the update script. It should work for the most part, but use at your own risk. I'll push it to the main branch once I've tested it. 

# Current items

Currently there is a mod updater in works. This is meant for running minetest in server mode on a vps. This is something that is being developed for my own uses and might nbe useful.
