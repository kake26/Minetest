#!/bin/bash


basedir=/home/kake26/minetest-5.10.0
worlddir=/home/kake26/minetest/minetest-server/.minetest/worlds/world2
confdir=/home/kake26/minetest
# script to start and stop minetest server


if [[ $1 == "start" ]] 
then
	echo "STARTING"
        $basedir/bin/minetestserver --quiet --logfile $basedir/minetest.log --config $confdir/conf/minetest.conf --world $worlddir  &
        pid=$!
        echo $pid > $basedir/mintest.pid
fi

if pidof minetestserver  
then
   # Horribly cheap but works
   echo "OK"
else
   # Seriously why not
   $basedir/bin/minetestserver --quiet --logfile $confdir/minetest.log --config $confdir/conf/minetest.conf --world $worlddir  &
fi

if [[ $1 == "stop" ]]
then
	echo "STOPING"
        $(pidof minetestserver | xargs kill -s INT)
fi
exit 0

