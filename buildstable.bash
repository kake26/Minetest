#!/bin/bash
# wget https://github.com/minetest/minetest/archive/master.tar.gz
wget https://github.com/minetest/minetest/archive/refs/tags/5.10.0.tar.gz
tar xf 5.10.0.tar.gz
cd minetest-5.10.0
cd games/
wget https://github.com/minetest/minetest_game/archive/master.tar.gz
tar xf master.tar.gz
mv minetest_game-master minetest_game
cd ..
cd lib/
wget https://github.com/minetest/irrlicht/archive/master.tar.gz
tar xf master.tar.gz
mv irrlicht-master irrlichtmt
cd ..
cmake . -DRUN_IN_PLACE=TRUE -DBUILD_SERVER=TRUE -DBUILD_CLIENT=FALSE -DCMAKE_BUILD_TYPE=Release
make -j$(nproc)
